FROM clux/muslrust:1.79.0-stable

RUN git clone https://github.com/jedisct1/libsodium
RUN cd libsodium && ./autogen.sh -s && ./configure --prefix=/usr && make -j$(nproc) install
RUN git clone https://github.com/facebook/zstd.git
RUN cd zstd/lib && make -j$(nproc) install
RUN git clone https://github.com/Cyan4973/xxHash
RUN cd xxHash && make -j$(nproc) install

WORKDIR /volume
