# pijul-musl

Unofficial builds of [pijul](https://pijul.org) on top of [MUSL LIBC](https://musl.libc.org/), directly from the Pijul Source code available on [nest](https://nest.pijul.org/pijul/pijul).

All produced binaries are available in the [Package Registry](https://gitlab.com/Kores/pijul-musl/-/packages), they are statically linked binaries and can be safely used in any distribution without the need to install additional dependencies.

**Note:** the pijul binary was removed from the repo, now the pipeline queries the binary from the [latest job artifacts](https://gitlab.com/Kores/pijul-musl/builds/artifacts/master/browse?job=buildpijul), it may help with future changes in the format to not break the pipeline (like it did and I didn't bothered fixing it for months).
**Note2:** The binary is back on the repo temporarily because of a sneaky bug that caused fresh clones to break in a very specific version of pijul, I didn't bothered removing it again because it's working.

## Goal

The goal of this project is to produce statically linked binaries of pijul, built directly from the source code, that can be executed in Docker Container and in various
linux distros without the need to install additional dependencies. 

## Schedule

The pipeline used to run once a day, but given that pijul development slowed down a bit, I've changed it to build once a week.

This may change but my aim is to have the latest pijul build always available, which means that the less frequent I'll have it running is twice a month, never less than that, unless, well, pijul development stalls for a long period.

## Build process

### Dependencies

In order to build pijul, [zstd](https://github.com/facebook/zstd) and [xxhash](https://github.com/Cyan4973/xxHash/) needs to be built as well (using musl), which adds a
certain friction to building pijul on musl-based environments, so this project pipelines builds those libs as well and pushes the produced Docker Image to [Container Registry](https://gitlab.com/Kores/pijul-musl/container_registry/2323927), this image can be used to build pijul for MUSL target.

### Building

#### Requirements
- latest [pijul](https://crates.io/crates/pijul)
  - Doesn't matter if it's musl or glibc based, will be only needed to clone the pijul repo.
  - It does not **need** to be the latest version, but the pijul format may still change in the beta channel, so trying to clone may fail if your pijul binary is outdate.
- docker
- git

#### Steps

Clone this repo:

```bash
git clone https://gitlab.com/Kores/pijul-musl
cd pijul-musl
```

Build the docker image:

```bash
docker build -t pijul-musl .
```

Clone the pijul repository:

**If you have a nest user**
```bash
pijul clone USER@nest.pijul.com:pijul/pijul
```

**If you don't have a nest user**
```bash
pijul clone https://nest.pijul.com/pijul/pijul
```

Launch the docker image:

```bash
docker run -it -v $PWD/pijul:/volume pijul-musl:latest cargo build --release
```

Now the musl based pijul build will be available at: `$PWD/pijul/target/x86_64-unknown-linux-musl/release/pijul`.
